# Car Controller
Electric Car Toy Controller.

## Description
This project was built so there exist an open source alternative to the motor controller
included in Electric Toy Cars that are built in to cars like [this one](https://images-na.ssl-images-amazon.com/images/G/31/apparel/rcxgs/tile._CB483369979_.gif)

## Background
I became frustrated that there was no suitable commercial solutions to integrate it easily to an existing 
toy, online solutions do exist such as [this](https://hobbyking.com/en_us/turnigy-30a-brushed-esc.html) but their documentation was a bit lacking for me, so I decided to roll out my own solution as I wanted to integrate a couple
of extra features to their functionality such as:

- Remote control features (reverse, move forward, brake)

## Badges
![test](https://img.shields.io/badge/Dev-In%20Progress-blue)


## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Installation
This project was developed using the following tools:

- The schematics and PCBs were designed in [Altium Designer 21](https://www.altium.com/products/downloads), I believe this should be backwards compatible with previous Altium versions, but I can not confirm it
- Microcontroller code was developed in [Microchip Studio (formerly Atmel Studio)](https://www.microchip.com/en-us/development-tools-tools-and-software/microchip-studio-for-avr-and-sam-devices) using the AVR GNU Toolchain not the MPLAB XC8 compiler as it offers step by step debugging, and other very useful debugging features
- PCBs are going to be fabricated with [PCB WAY](https://www.pcbway.com), they are an OK price for prototypes (TODO) 

## Usage
- Open the Microchip Studio project and do a clean build to generate the hex file according to your AVR controller of choice, I used an Atmega328 variant
- Burn the microcontroller code using your favorite programmer, I used an [Atmel ICE](https://www.microchip.com/en-us/development-tool/ATATMEL-ICE)
- Open the Altium project to generate the gerber files to send them over to your favorite fabrication house, or if plan on doing a heat transfer method, modify the outputs accordingly

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
Big kuddos to folks at the AVR Freaks for sharing a lot of their knowledge for free

## License
This project is released under GNU General Public License v3.0.

## Project status
In Progress
