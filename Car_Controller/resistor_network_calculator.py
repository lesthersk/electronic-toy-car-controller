# Welcome! This file was intended to help out with the selection of resistors
# since usually buck or boost converter datasheets tend to choose odd values
# adjust the following values to your particular need
import pandas

import common_resistor_values_calculator as crv

# equation = lower_resistor * ((target_voltage / minimum_voltage) - 1)  # original feedback equation
target_voltage = 5  # target output voltage
margin_of_error = 0.05  # margin of error in %
minimum_voltage = 0.925  # internal voltage reference
target_tolerance = 'tolerance_group_1'  # modify this according to the table below


# tolerance_group_1 includes tolerances of 0.1, 0.25, 0.5%
# tolerance_group_2 includes tolerances of 1%
# tolerance_group_3 includes tolerances of 2, 5%
# tolerance_group_4 includes tolerances of 10%

def main():
    print('\nCalculating matching resistor values...\n\n')
    available_resistor_values = crv.get_all_resistor_values()
    upper_resistor = available_resistor_values.get(target_tolerance)
    lower_resistor = available_resistor_values.get(target_tolerance)
    resistor_one = []
    resistor_two = []

    for upper_resistor_index in range(len(available_resistor_values.get(target_tolerance))):
        for lower_resistor_index in range(len(available_resistor_values.get(target_tolerance))):
            # equation arranged thanks to Wolfram Alpha! feel free to modify it at your convenience
            if abs(((37 * (upper_resistor[upper_resistor_index] + lower_resistor[lower_resistor_index]) /
                     (40 * lower_resistor[lower_resistor_index])) / target_voltage) - 1) < margin_of_error:
                resistor_one.append(upper_resistor[upper_resistor_index])
                resistor_two.append(lower_resistor[lower_resistor_index])

    data_in_dictionary_format = {'resistor_one': resistor_one, 'resistor_two': resistor_two}
    data_frame = pandas.DataFrame(data_in_dictionary_format)
    data_frame.to_csv('valid_resistor_combinations.csv', index=False)
    print('\n\nDone!\n')


if __name__ == "__main__":
    # execute only if run as a script
    main()
